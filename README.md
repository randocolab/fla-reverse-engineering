# Fla reverse engineering (VERY WIP)
## A PPP
The goal of this project is to reverse engineer adobe's Fla/XFL format to export in a more open format.
Current goal is SVG.
![Edge](https://u.smutty.horse/mbqjmvkteys.png)

The project is described in more detail in:
https://docs.google.com/document/d/1qv4JJP2jycpELOv4idkRt3elWIZDepHijfx3ZsKj_Mk/edit?usp=sharing

This repository serves to host the python code used to export FLA/XFL to a different format.

A (incomplete) reference guide to FLA/XFL can be found in the google doc.
