# Code
## Current experiments:
experiment_01.py -- Testing curve and lines segments.

Usage:

```
pip install -r requirements.txt
python3 experiment_01.py
```

## Other Files:
Symbol.py module required for reading symbol files AKA assets. ( VERY INCOMPLETE )

DOMDocument.py dummy module ATM

## Output
SVGs go here
