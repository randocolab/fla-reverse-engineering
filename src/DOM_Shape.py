import copy
import re
import svgwrite
import itertools


def hex_to_rgb(hex: str):
    """
    :param hex: e.g. '#E7001E'
    :return: R G B values in a list
    """
    return [int(hex[i:i + 2], 16) for i in (0, 2, 4)]


def toFloat(adobe_number: str, factor=1):
    """
    Adobe uses a odd number notation, examples:

    #FFFEE0.F7 -> 3033.30078125
    #1F3CE0.1 -> #1F3CE0.10 -> 102360.003125
    644S4 -> 644, just remove 'S4'  # Note: We actually removed this in the regex

    :param factor: Multiplication factor for down-scaling & up-scaling
    :param adobe_number
    :return: Regular floats
    """
    # adobe_number = adobe_number.replace(' ', '')
    if '#' in adobe_number:
        adobe_number = adobe_number.replace('#', '')
        if adobe_number[-2] == '.':  # Check if only one decimal
            # If only one decimal, add a zero. Else we get different numbers.
            # 1F3CE0.1 -> 1F3CE01 -> 6397.5001953125
            # 1F3CE0.10 -> 1F3CE010 -> 102360.003125
            adobe_number += '0'
        adobe_number = int(adobe_number.replace('.', ''), 16)
        if adobe_number >= 0x80000000:
            # https://github.com/SasQ/SavageFlask/blob/master/src/XFL/edge.rb#L32
            # I don't even want to attempt to remember what happens here.
            adobe_number = -(0xFFFFFFFF - adobe_number + 1)
        return adobe_number / 256 / 20 * factor
    else:
        return float(adobe_number) / 20 * factor


def parse_edge(code: str):
    """
    Parses opcodes with arguments as strings as a list e.g.
    [
    '!', #Opcode, with arguments below: Move to
    '120',
    '20'
    '[',  #Opcode, with arguments below: Curve to
    '#17F.6',
    '#FFFE73.92',
    '1400',
    '0'
    ]

    # TODO move this doc to other function that finds loops.
    Then finds edge loops, which is when the end destination equals the beginning.
     E.g. Take the next Edge point list for example
      '!295 -187.5[#1A6.F -131.96875 425 -140.5!425 -140.5|425 -194!425 -194[#15B.9 #FFFF42.16 295 -187.5'
      '!295 - 187.5' and '[#15B.9 #FFFF42.16 295 -187.5' have the same control points (295 -187.5)
      Which means it is a loop, this can be different opcodes
      '!1800 81/2240 81!2240 81/2240 200!2240 200/1800 200!1800 200/1800 81'
      '!1800 81' and '/1800 81' have same points and is so a loop

      Note: S[number] in the first opcode e.g. "!0 200S2|0 0" stands for being selected.
      Which can be ignored.
    :returns: Splitted, based on loop
    """
    # code = list(map(lambda edges: edges.replace('  ', ' '), code))
    # Puts move in serpate group regexpr = '([!/|\[\]])(-?[#0-9A-F.]+) (-?[#0-9A-F.]+) ?(?:S\d)?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)?',
    # Includes commands move_to included in group: '([!/|\[\]])(-?[#0-9A-F.]+) (-?[#0-9A-F.]+)([ \|\/\[\]]?)(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)?'
    # White space (?:[!/|\[\]])(-?[#0-9A-F.]+) (-?[#0-9A-F.]+)(?:[ \|\/\[\]]?)(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)?
    # Account for S6, S5, ( Object selection? )
    edge_points = re.findall(
        '(?:[!/|\[\]])(?: +)?(-?[#0-9A-F.]+)(?: +)?(-?[#0-9A-F.]+)(?: +)?(?:S\d)?(?:[ \|\/\[\]]?)(?: +)?(-?[#0-9A-F.]+)?(?: +)?(-?[#0-9A-F.]+)?(?: +)?(-?[#0-9A-F.]+)?(?: +)?(-?[#0-9A-F.]+)?',
        code)
    # This regex will make your eyes bleed.
    #   I had to account for bullshit like the XML parser
    #   inserting a whitespace when it encountered a newline in a attribute value

    # Remove None's,
    edge_points = [list(filter(None, edge)) for edge in edge_points]

    # convert adobe hex 2's complement numbers to regular floats.
    edge_points = [list(map(toFloat, list(filter(None, opcode)))) for opcode in
                   edge_points]
    # split points into x,y sub lists.
    x = 2  # == len([x,y])
    edge_points = list(map(lambda edge: [edge[i:i + x] for i in range(0, len(edge), x)], edge_points))
    # If list length == 3 then it's a curve to, else it's an line to, that's all.
    return edge_points


def parse_edges(DOMShape):
    return [{'edges': parse_edge(edge.getAttribute('edges')),
             'fillStyle1': edge.getAttribute('fillStyle1'),
             'fillStyle0': edge.getAttribute('fillStyle0'),
             'strokeStyle': edge.getAttribute('strokeStyle')} for edge in DOMShape]


def parse_style(styles, e):
    return {style.getAttribute('index'): solid_color.getAttribute('color') for style in styles
            for solid_color in style.getElementsByTagName('SolidColor')}


def find_loops(unlooped_lines):
    """
    Connects grouped-by-fillstyle draw commands to loops
    """
    for fill, path in unlooped_lines.items():  # Check if end matches begin, and is thus a connected loop
        edge_loops = [[path[0]]]
        path.pop(0)
        while path:
            for idx_1, cmd_1 in enumerate(path):

                if edge_loops[-1][-1][-1] == cmd_1[0]:
                    if not edge_loops[-1][-1] == cmd_1[::-1]:
                        #  + -> +
                        # Point to connected check
                        # print(F"C {edge_loops[-1][-1][-1], cmd_1[0]}")
                        # print(F"Index: {idx_1}")
                        edge_loops[-1].append(cmd_1)
                        path.pop(idx_1)
                        break
                if edge_loops[-1][0][0] == edge_loops[-1][-1][-1]:
                    # + -> +
                    # ^    |
                    # |    v
                    # + <- +
                    # Loop connected check
                    # print("L")
                    edge_loops.append([path[0]])
                    path.pop(0)
                    break

                #  We check if it matches previous, cmd
                #  We check if the loop connects, so that start point == End point

        unlooped_lines[fill] = edge_loops
    return unlooped_lines


def group_fills(symbol_paths):
    """
    Lots of code duplication here. TODO: Clean up
    This func. groups edges by the their fill.
    """

    lines_fills = {}
    lines_strokes = {}

    for idx, path in enumerate(symbol_paths):  # Separation of Strokes of fills and lines
        if path['fillStyle1']:
            if path['fillStyle1'] not in lines_fills:
                lines_fills[path['fillStyle1']] = path['edges']
            else:
                lines_fills[path['fillStyle1']] += path['edges']
        if path['fillStyle0']:
            if path['fillStyle0'] not in lines_fills:
                lines_fills[path['fillStyle0']] = [path['edges'][0][::-1]]
            else:
                lines_fills[path['fillStyle0']] += [path['edges'][0][::-1]]

        if path['strokeStyle']:
            if path['strokeStyle'] not in lines_strokes:
                lines_strokes[path['strokeStyle']] = [path['edges']]
            else:
                lines_strokes[path['strokeStyle']] += [path['edges']]

    fills_loop_list = find_loops(lines_fills)
    # lines_strokes = find_loops(lines_strokes)

    return fills_loop_list, lines_strokes


class DOM_Shape:
    def __init__(self, DOMShape):
        FillStyle = DOMShape.getElementsByTagName('FillStyle')
        StrokeStyle = DOMShape.getElementsByTagName('StrokeStyle')
        edges = [Edge_dom for Edge_dom in DOMShape.getElementsByTagName('Edge') if
                 Edge_dom.getAttribute('edges')]

        self.style_fill = parse_style(FillStyle, 'FillStyle')
        self.style_line = parse_style(StrokeStyle, 'StrokeStyle')
        self.path = parse_edges(edges)
        self.fills = group_fills(copy.deepcopy(self.path))
        pass

    def add_svg_elements(self, Drawing: svgwrite.Drawing, style=None, DBG=False):
        """
         :param parse_edge: Result from parse_edge.
        :param Drawing: svgwrite.Drawing to add elements to
        :param style: SVG Styling information
        :return:
        """
        # https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
        # https://pythonfix.com/code/svgwrite_code_examples/
        # if not (self.style_line or self.style_fill):

        style = {"stroke-width": "1",
                 "stroke-linejoin": "round",
                 "stroke-linecap": "round"}  # I call this clever

        for style_idx, loops in self.fills[0].items():  # Render Fills
            style.update({"fill": self.style_fill[style_idx]})
            self.add_elements(Drawing, style, loops)

        style['fill'] = "none"

        for stroke in self.path:
            if stroke['strokeStyle']:
                style.update({"stroke": self.style_line[stroke['strokeStyle']]})
                self.add_elements(Drawing, style, [stroke['edges']], Stroke=True)

        for style_idx, loops in self.fills[0].items():  # Render Fills
            style.update({"stroke": self.style_line[style_idx]})
            self.add_elements(Drawing, style, loops, Stroke=True)

    def add_elements(self, Drawing, style, loops, Stroke=False, DBG=False):

        for edge in loops:
            # if self.style_fill.get(E_style):
            #     style['fill'] = self.style_fill[E_style]

            SVG_path_cmd = ""
            for cmd in edge:
                # ----------DEBUG points----------
                if DBG:
                    for i in cmd:  # Draw circles on every point
                        Drawing.add(Drawing.circle(center=(i[0], i[1]),
                                                   r=.1,
                                                   stroke=svgwrite.rgb(15, 15, 15, '%'),
                                                   fill='white'))
                # ----------DEBUG points----------
                if not SVG_path_cmd or Stroke:
                    SVG_path_cmd += "M{} {} ".format(*list(itertools.chain.from_iterable(cmd))[:2])

                if len(cmd) == 2:  # Line to
                    SVG_path_cmd += "L{} {} ".format(*list(itertools.chain.from_iterable(cmd))[2:])
                    # Flatten list and put it into put them in as positional arguments
                if len(cmd) == 3:  # Curve to
                    SVG_path_cmd += "Q{} {} {} {} ".format(*list(itertools.chain.from_iterable(cmd))[2:])

            # SVG_path_cmd += "Z"

            svg_cmd = Drawing.path(SVG_path_cmd,
                                   **style)  # Use dict as style information, and use them as keyword arguments
            if SVG_path_cmd:
                Drawing.add(svg_cmd)
            else:
                raise

    def render_edge(self, **kwargs):
        if kwargs.get('SVG'):
            return self.SVG_write(kwargs.get('SVG'))
