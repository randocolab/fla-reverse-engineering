import time
from xml.dom import minidom

import os
from pathlib import Path
from xml.dom.minidom import parse
import svgwrite
import DOM_Shape
import xmltodict

# def count_unique_elements_and_attributes_value(Symbol_folder):
#     """
#     element e.g = 'DOMTimeline'
#     attribute = 'name'
#     value = 'Symbol 3'
#
#     Run against a extracted symbol folder, it will give you a vague idea what elements
#     and attributes will have to be supported to properly render a FLA project scene.
#     :return:
#     """
#     from collections import Counter
#     c = Counter()
#     b = Counter()
#     for subdir, dirs, files in os.walk(Symbol_folder):
#         for file in files:
#             symbol = etree.parse(os.path.join(subdir, file))
#             for attr in symbol.iter():
#                 b.update([attr.tag])
#                 items = attr.items()
#                 i = list(itertools.chain.from_iterable(items))
#                 print(i)
#                 c.update(i)
#
#     print(c.most_common(20))
#     print(b.most_common(20))

P = Path("../data/XFL/RECOVER_Untitled-1_20216144486/LIBRARY/wonderbolt1_line.xml")
symbols = os.listdir(P)


def render_symbol(symbol_path, canvas_args=None):
    XML = parse(str(symbol_path))
    for timeline in XML.getElementsByTagName('timeline'):
        for DOMLayer in timeline.getElementsByTagName('DOMLayer'):
            layer_name = DOMLayer.getAttribute('name')
            print('|-Layer:', layer_name)
            for DOMFrame in DOMLayer.getElementsByTagName('DOMFrame'):
                frame_idx = DOMFrame.getAttribute('index')
                print('||-Frame:', frame_idx)
                svg_drawing = svgwrite.Drawing(
                    filename=str(Path('output') / (symbol_path.name[:-4] + '_' + frame_idx + '.svg')),
                    size=None,
                    **canvas_args)

                for DOMShape in DOMFrame.getElementsByTagName('DOMShape'):
                    # FillStyle = DOMShape.getElementsByTagName('FillStyle')
                    # StrokeStyle = DOMShape.getElementsByTagName('StrokeStyle')
                    # edges = [Edge_dom for Edge_dom in DOMShape.getElementsByTagName('Edge') if
                    #          Edge_dom.getAttribute('edges')]
                    # # edges, StrokeStyle, FillStyle

                    shape = DOM_Shape.DOM_Shape(DOMShape)
                    shape.add_svg_elements(svg_drawing)

                return svg_drawing


DBG = False  # Debug global

canvas_args = {"viewBox": "0 0 100 100"}
# for filename in os.listdir(P)[1:2]:
#     if filename.endswith(".xml"):
#         print("New Symbol: ", filename)
for i in enumerate(os.listdir(P)):
    print(i)

filename = os.listdir(P)[1]
file_path = P / filename
Z = xmltodict.parse(open(file_path).read())
print(filename)
render_symbol(file_path, canvas_args).save()
