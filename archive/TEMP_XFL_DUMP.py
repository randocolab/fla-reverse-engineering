import bpy
from xml.dom.minidom import parse
from itertools import chain
from functools import partial
import re

""""
Converts Adobe Flash/Animate XFL project files to blender files WIP.
Reverse engineered XFL EDGE documentation:

# https://github.com/SasQ/SavageFlask/blob/master/doc/FLA.txt
# https://stackoverflow.com/questions/4077200/whats-the-meaning-of-the-non-numerical-values-in-the-xfls-edge-definition

# !(x,y) moveTo
# /(x,y)+ lineTo
# |(x,y)+ lineTo
# [(x1 y1 ex ey)+ curveTo (quadratic)
# ](x1 y1 ex ey)+ curveTo (quadratic)
# ((pBCPx pBCPy)? ; x1 y1 x2 y2 ex ey (({Q,q,P,p})? x y)+ curveTo (cubic start)
# )(nBCPx nBCPy)? ; curveTo (cubic end)
# Sn selection (n=bitmask, 1:fillStyle0, 2:fillStyle1, 4:stroke)
# #aaaaaa.bb is a signed fixed point 32 bit number

Development notes:
Using classes to organize functions.
I feel like emptying the contents of my stomach.  
"""

__author__ = 'Blender/Animation Anon.'
__version__ = .7

win = bpy.context.window
scr = win.screen
areas3d = [area for area in scr.areas if area.type == 'VIEW_3D']
region = [region for region in areas3d[0].regions if region.type == 'WINDOW']
override = {'area': areas3d[0]}


def reset_blend():
    """
    For testing purposes
    :return:
    """
    context = bpy.context
    scene = context.scene

    for c in scene.collection.children:
        scene.collection.children.unlink(c)

    for bpy_data_iter in (
            bpy.data.objects,
            bpy.data.curves,
            bpy.data.grease_pencils
    ):
        for id_data in bpy_data_iter:
            bpy_data_iter.remove(id_data)
    bpy.ops.outliner.orphans_purge()


def hex_to_rgb(hex: str):
    """
    :param hex: e.g. '#E7001E'
    :return: R G B values in a list
    """
    return [int(hex[i:i + 2], 16) for i in (0, 2, 4)]


class edge_opcode:
    code_dict = {
        '!': 'move',
        '/': 'line',
        '|': 'line',
        '[': 'curve',
        ']': 'curve',
    }

    def create(self, x, y):
        """
        Note: this method is temporary and will be replaced with a more efficient version
        that let's us add splines directly to grease pencil objects.
        https://devtalk.blender.org/t/gsoc-2020-editing-grease-pencil-strokes-using-curves-feedback-suggestions/12350/5
        """

        if self.opcode == 'line':
            bpy.data.curves[-1].splines.new('POLY')
            # Don't ask me why but creating a new spline also creates a point, so add /one/ less.
            bpy.data.curves[-1].splines[-1].points.add(1)
            bpy.data.curves[-1].splines[-1].points[-2].co = [x, y, 0, 1]
            bpy.data.curves[-1].splines[-1].points[-1].co = [*self.control_points, 0, 1]
        if self.opcode == 'curve':
            bpy.data.curves[-1].splines.new('NURBS')
            bpy.data.curves[-1].splines[-1].points.add(2)
            bpy.data.curves[-1].splines[-1].points[-3].co = [x, y, 0, 1]
            bpy.data.curves[-1].splines[-1].points[-2].co = [*self.curve_point, 0, 1]
            bpy.data.curves[-1].splines[-1].points[-1].co = [*self.control_points, 0, 1]
            bpy.data.curves[-1].splines[-1].use_endpoint_u = True
        # NOTE: Don't forget to save moveTo yourself and pass them as argument!

    def __str__(self):
        return \
            F'Co:{self.control_points}   ' \
            F'Cu:{self.curve_point}  ' \
            F'{self.opcode}  '

    def __init__(self, code):
        self.opcode = self.code_dict[code[0]]

        if self.opcode == 'move' or self.opcode == 'line':
            self.control_points = code[1:3]
            self.curve_point = None
        else:
            self.control_points = code[3:]
            self.curve_point = code[1:3]


class XFL_Edge:

    def __init__(self, name):
        self.name = name
        # Regex for cubic = '!-?(\d+) -?(\d+)\(;-?(\d+),-?(\d+) -?(\d+),-?(\d+) -?(\d+),-?(\d+)([Q,q,P,p, ])-?(\d+) -?(\d+)([Q,q,P,p, ])-?(\d+) -?(\d+)([Q,q,P,p, ])-?(\d+) -?(\d+)\);'

    def GP_merge_as_layer(self, GP_object_name, transfer_to_name):
        """
        # Merge last grease pencil objects into one with multiple layers.
        # And remove the temp. GP object
        :return:
        """
        GP_object = bpy.data.objects[GP_object_name]
        bpy.context.view_layer.objects.active = GP_object
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.gpencil.layer_duplicate_object(object=transfer_to_name)
        # bpy.ops.gpencil.layer_duplicate_object(object=bpy.context.objects_in_mode[-2].name)  # Fixme: Breaks?
        # bpy.context.object
        # bpy.ops.object.mode_set(mode='OBJECT')
        # bpy.ops.object.delete()

    def connect_loops(self, loops):
        for loop1 in loops:
            for loop2 in loops:
                if loop1[0].control_points == loop2[-1].control_points:
                    print("Possible loop?")

    def find_filledRegions(self):
        edges


    def find_contours(self, edge_lists):
        # edges = list(chain.from_iterable(edge_lists))  # flatten the list

        edges = [edge_opcode(edge_co) for edge_co in edge_lists]  # We could move this to parse.

        edge_loops = []

        # next(edge_op for index, edge_op in enumerate(edges) if edge_op.control_points == edges[index + 1:])

        #  Algorithm that finds contours.
        #  TODO: check performance cost of creating edge objects instead of old approach.
        #  Find matching control points, splice edges remove the spiced edges from 'edges' variable.

        index = 0
        while edges and index != len(edges) - 1:
            for index, edge_op in enumerate(edges):
                if edge_op.control_points == edges[0].control_points and index:
                    print(index, edge_op)
                    edge_loops.append(edges[:index + 1])
                    edges = edges[index + 1:]  # Fixme index + 1?
                    break

        if edges and index > 0:  # left overs?
            edge_loops.append(edges)
        else:  # No loops found.
            edge_loops = edges

        # Find_loop:
        # Check if control points match (see function doc)
        # '!1800 81'     and '/1800 81'
        #  or
        # '!295 - 187.5' and '[#15B.9 #FFFF42.16 295 -187.5'
        # find_loop = lambda opcode: opcode[1:3] == edges[index][1:3] or \
        #                            opcode[3:] == edges[index][1:3]
        #
        # # # FIXME: Correct for _most_ cases ( I don't know all cases so just putting that here ).
        # # Note:
        # # THIS
        # # COULD
        # # PROBABLY
        # # HAVE
        # # BEEN
        # # DONE
        # # SO
        # # MUCH
        # # EASIER.
        #
        # last_index = 0
        # index = 0
        # edge_points_parsed = []
        # try:
        #     while next(filter(find_loop, edges[index + 1:])):
        #         index = edges.index(next(filter(find_loop, edges[index + 1:])))
        #
        #         to_add_edges = edges[last_index: index + 1]
        #         # if index:
        #         #     to_add_edges.insert(0, edges[index - 1])
        #         edge_points_parsed.append(to_add_edges)
        #
        #         if index == last_index:
        #             index += 1
        #         last_index = index
        #
        # except:  # End reached of list
        #     if edge_points_parsed:  # do we have parsed anything?
        #         if index + 1 != len(edges) and index + 1 < len(edges):
        #             edge_points_parsed.append(edges[index + 1:])  # add the remaining edges.
        #     elif not edge_points_parsed:  # A non loop
        #         edge_points_parsed.append(edges)
        return edge_loops

    def parse_edge(self, code: str):
        """
        Parses opcodes with arguments as strings as a list e.g.
        [
        '[',  #Opcode, with arguments below.
        '#17F.6',
        '#FFFE73.92',
        '1400',
        '0'
        ]

        # TODO move this doc to other function that finds loops.
        Then finds edge loops, which is when the end destination equals the beginning.
         E.g. Take the next Edge point list for example
          '!295 -187.5[#1A6.F -131.96875 425 -140.5!425 -140.5|425 -194!425 -194[#15B.9 #FFFF42.16 295 -187.5'
          '!295 - 187.5' and '[#15B.9 #FFFF42.16 295 -187.5' have the same control points (295 -187.5)
          Which means it is a loop, this can be different opcodes
          '!1800 81/2240 81!2240 81/2240 200!2240 200/1800 200!1800 200/1800 81'
          '!1800 81' and '/1800 81' have same points and is so a loop

          Note: S[number] in the first opcode e.g. "!0 200S2|0 0" stands for being selected.
          Which can be ignored.
        :returns: Splitted, based on loop
        """
        # code = list(map(lambda edges: edges.replace('  ', ' '), code))
        code = code.replace('  ', ' ')
        edge_points = re.findall(
            '([!/|\[\]])(-?[#0-9A-F.]+) (-?[#0-9A-F.]+) ?(?:S\d)?(-?[#0-9A-F.]+)? ?(-?[#0-9A-F.]+)?',
            code)

        # Remove None's, convert adobe hex 2's complement numbers to regular floats.
        edge_points = [list(opcode[:1]) + list(map(self.toFloat, list(filter(None, opcode[1:])))) for opcode in
                       edge_points]

        return edge_points

    @staticmethod
    def toFloat(adobe_number: str, factor=.3):
        """
        Adobe uses a odd number notation, examples:

        #FFFEE0.F7 -> 3033.30078125
        #1F3CE0.1 -> #1F3CE0.10 -> 102360.003125
        644S4 -> 644, just remove 'S4'  # Note: We actually removed this in the regex

        :param factor: Multiplication factor for down- or upscaling
        :param adobe_number
        :return: Regular floats
        """
        # adobe_number = adobe_number.replace(' ', '')
        if '#' in adobe_number:
            adobe_number = adobe_number.replace('#', '')
            if adobe_number[-2] == '.':
                # If only one decimal, add a zero. Else we get different numbers.
                # 1F3CE0.1 -> 1F3CE01 -> 6397.5001953125
                # 1F3CE0.10 -> 1F3CE010 -> 102360.003125
                adobe_number += '0'
            adobe_number = int(adobe_number.replace('.', ''), 16)
            if adobe_number >= 0x80000000:
                # https://github.com/SasQ/SavageFlask/blob/master/src/XFL/edge.rb#L32
                adobe_number = -(0xFFFFFFFF - adobe_number + 1)
            return adobe_number / 256 / 20 * factor
        else:
            return float(adobe_number) / 20 * factor

    def create_fills(self, style_type, style_index, loops):
        """

        Fills:
        Since we can't connect NURBS curves to poly curves we have to iterate over every opcode, add it. convert it
        to grease pencil, connect it with the previous line, and in the end toggle cyclic if it is a loop.
        And apply material

        :param loops:
        :return:
        """

        global override



        self.x, self.y = [0, 0]
        for loop in loops:
            print('--New loop--')
            for index, edge in enumerate(loop):
                bpy.data.curves[-1].splines.clear()
                edge.create(self.x, self.y)
                if edge.opcode == 'move':
                    self.x, self.y = edge.control_points

                pass
                #  Converting to GP and closing loop
                if edge.opcode != 'move':  # If not the first point
                    bpy.ops.object.mode_set(mode='OBJECT')
                    bpy.data.objects['Temp'].select_set(True)
                    bpy.context.view_layer.objects.active = bpy.data.objects['Temp']
                    bpy.ops.object.convert(target='GPENCIL', keep_original=True)  # Convert to grease pencil
                    bpy.context.view_layer.objects.active = bpy.data.objects['GPencil']
                    bpy.ops.object.mode_set(mode='EDIT_GPENCIL')
                    # if index:
                    bpy.ops.gpencil.layer_duplicate_object(object=self.name)  # Move layer to main GP layer
                    bpy.data.grease_pencils.remove(bpy.data.grease_pencils.get('GPencil'))  # Delete temp GP object with data block

                    bpy.context.view_layer.objects.active = bpy.data.objects[self.name]
                    bpy.ops.object.mode_set(mode='EDIT_GPENCIL')
                    bpy.ops.gpencil.layer_merge()                # Merge layers
                    if index and style_type != 'strokeStyle':
                        bpy.ops.gpencil.select_all(action='SELECT')  # Select both strokes
                        bpy.ops.gpencil.stroke_join(override)       # join strokes
                    # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
                    pass
    def create_strokes(self, style_type, style_index, loops):
        """
        Strokes:
        For each opcode, add it, convert to grease pencil and apply material.
        (Let's hope this doesn't affect performance too badly)

        :loop A dict
        :return: None
        """
        self.x, self.y = [0, 0]
        bpy.data.curves[-1].splines.clear()
        for loop in loops:
            print('--New loop--')
            for index, edge in enumerate(loop):
                edge.create(self.x, self.y)
                if edge.opcode == 'move':
                    self.x, self.y = edge.control_points

                if index != 0:
                    bpy.ops.object.convert(target='GPENCIL', keep_original=True)
                    bpy.ops.gpencil.layer_duplicate_object(object="GPencil")
                    #  Change active?
                    bpy.ops.object.delete(use_global=False)
                    bpy.ops.gpencil.stroke_join(type='JOIN')
        # for loop in code_list:
        # point_list = []
        # self.x, self.y = [0, 0]
        # for loop in loops:
        #     for opcode in loop:
        #
        #         # float_arguments = list(map(self.toFloat, list(filter(None, opcode[1:]))))
        #         if opcode.opcode == 'line':
        #             # if opcode[0] in ['/', '|']:
        #             bpy.data.curves[-1].splines.new('POLY')
        #             bpy.data.curves[-1].splines[-1].points.add(1)
        #             bpy.data.curves[-1].splines[-1].points[-2].co = [self.x, self.y, 0, 1]
        #             bpy.data.curves[-1].splines[-1].points[-1].co = [*opcode.control_points, 0, 1]
        #
        #             # bpy.data.curves[-1].splines[-1].points[-2].co = [self.x, self.y, 0, 1]
        #             # bpy.data.curves[-1].splines[-1].points[-1].co = [*opcode[1:3], 0, 1]
        #
        #             # one = bpy.data.objects[-1]
        #             # one.data.splines[-1].points[0].select = True
        #             #
        #             # bpy.ops.curve.dissolve_verts()
        #             # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #
        #             # point_list.append([[self.x, self.y, 0, 1], [*opcode[1:3], 0, 1]])
        #         # if opcode[0] in ['[', ']']:
        #         if opcode.opcode == 'curve':
        #             bpy.data.curves[-1].splines.new('NURBS')
        #             # bpy.data.curves[-1].splines[-1].use_bezier_u = True
        #             bpy.data.curves[-1].splines[-1].points.add(2)
        #             bpy.data.curves[-1].splines[-1].points[-3].co = [self.x, self.y, 0, 1]
        #             bpy.data.curves[-1].splines[-1].points[-2].co = [*opcode.curve_point, 0, 1]
        #             bpy.data.curves[-1].splines[-1].points[-1].co = [*opcode.control_points, 0, 1]
        #
        #             # bpy.data.curves[-1].splines[-1].points[-2].co = [*opcode[1:3], 0, 1]
        #             # bpy.data.curves[-1].splines[-1].points[-1].co = [*opcode[3:], 0, 1]
        #
        #             bpy.data.curves[-1].splines[-1].use_endpoint_u = True
        #
        #             # one = bpy.data.objects[-1]
        # one.data.splines[-1].points[0].select = True
        #
        # bpy.ops.curve.dissolve_verts()
        # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        # print(opcode)
        # pass
        # bpy.context.scene.tool_settings.gpencil_selectmode_edit = 'STROKE'
        # bpy.ops.object.convert(target='GPENCIL')
        # bpy.ops.gpencil.editmode_toggle()
        # bpy.ops.gpencil.select_all(action='SELECT')
        # bpy.ops.gpencil.stroke_join(type='JOIN')

        # bpy.data.curves[-1].splines[-1].points[4].co = [*opcode[3:], 0, 1]
        # bpy.data.curves[-1].splines[-1].points[6].co = [*opcode[3:], 0, 1]
        # point_list.append([[self.x, self.y, 0, 1],
        #                    [*opcode[1:3], 0, .5],
        #                    [*opcode[3:], 0, 1]])

        # if opcode[0] == '!':
        #     # point_list.append([*opcode[1:3], 0, 1])
        #     self.x, self.y = opcode[1:3]

        #
        # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        # print(opcode)
        # print('----')
        # pass

        # Set 'one' to edit mode
        # bpy.context.scene.objects.active = one

        # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #     bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        # print(opcode)
        # pass

        #
        # for p, new_co in zip(bpy.data.curves[-1].splines[-1].points,
        #                      point_list):  # Technically it optionally a loop, but it usually is.
        #
        #     p.co = new_co

        # Convert opcode 'arguments' to floats and remove 'None''s

        # print(float_arguments)

        # If the opcode is in the opcode function dict, call the function that it's related to with the converted arguments.
        # https://stackoverflow.com/questions/2974022/is-it-possible-to-assign-the-same-value-to-multiple-keys-in-a-dict-object-at-onc
        # next(v for k, v in self.opcode.items() if opcode[0] in k)(point)
        # assign the point coordinates to the spline points

        # bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

        # # Set 'one' to edit mode
        # if code_list[0][1:] == code_list[-1][1:]:  # DOES IT LoOoOoP?
        # bpy.data.curves.get('Temp').splines[-1].points.add(len(point_list) - 1)

        # bpy.context.view_layer.objects.active = bpy.data.objects.get('Temp')
        # bpy.ops.object.mode_set(mode='EDIT')
        # # Select point_to_remove and remove it
        # bpy.data.curves[-1].splines[-1].points[0].select = True
        # bpy.ops.curve.dissolve_verts()
        # bpy.ops.object.mode_set(mode='OBJECT')


class XFL_Symbol:
    """
    Adobe Symbols, contain drawings & other symbols.
    Types: Graphic, Movieclip
    Effectively a Grease pencil object with a lattice with child objects.
    """

    def parse_styles(self, DOMShape):
        Fills = DOMShape.getElementsByTagName("FillStyle")
        Strokes = DOMShape.getElementsByTagName("StrokeStyle")
        GP_materials = dict()

        for fillstyle in Fills:
            #  Convert Fillstyles to Grease Pencil Materials
            gp_mat = bpy.data.materials.new(fillstyle.getAttribute('index'))
            bpy.data.materials.create_gpencil_data(gp_mat)

            # Convert hex colour into RGB values

            colour = hex_to_rgb(fillstyle.childNodes[1].getAttribute('color').lstrip('#'))
            alpha = fillstyle.childNodes[1].getAttribute('alpha')
            if alpha:
                alpha = float(alpha)
            else:  # In case there is no alpha attribute, it is 1.
                alpha = 1.0
            gp_mat.grease_pencil.fill_color = [*colour, alpha]
            gp_mat.grease_pencil.show_stroke = False
            gp_mat.grease_pencil.show_fill = True
            GP_materials.update({fillstyle.getAttribute('index'): gp_mat})
        return GP_materials

    def strokes(self, DOMShape, DOMLayer, name):

        # GP_materials = self.parse_styles(DOMShape)
        # GP_data = bpy.data.grease_pencils.new(name)
        # bpy.data.objects.new(name, GP_data)

        edge_class = XFL_Edge(name)
        edges = [Edge_dom for Edge_dom in DOMShape.getElementsByTagName('Edge') if
                 Edge_dom.getAttribute('edges')]

        edge_styles = ['strokeStyle', 'fillStyle0', 'fillStyle1']

        # Rather complicated list comprehension, we simply store the style type
        edge_styles = \
            [[i,
                Edge_dom.getAttribute(i),
                    edge_class.find_contours(                 # 3. Get the contours for each "Edge" element.
                        edge_class.parse_edge(                # 2. Parse them
                            Edge_dom.getAttribute('edges')))  # 1. Get raw edges string

            ] for i in edge_styles for Edge_dom in edges if Edge_dom.getAttribute(i)]

        # edge_styles = {i: list(filter(lambda Edge_dom: Edge_dom.getAttribute(i), edges)) for i in edge_styles}
        # edge_styles = [list(map(lambda edges: edges.getAttribute('edges'), styles)) for styles in edge_styles]
        # edges = list(map(lambda edges: edges.replace('  ', ' '), edges))

        # edge_styles = [list(map(edge_class.parse_edge, edges)) for edges in edge_styles]
        # edges = [edge_class.find_contours(edges) for edges in edge_styles]
        # for

        for edge_style in edge_styles:
            if edge_style[0] == 'fillStyle1':
            # if edge_style[0] == 'strokeStyle':
                print('--- New Style list ---')
                edge_class.create_fills(*edge_style)


        # bpy.context.view_layer.objects.active = bpy.data.objects.get('Temp')
        # bpy.ops.object.mode_set(mode='OBJECT')
        # bpy.ops.object.convert(target='GPENCIL', keep_original=True)
        # bpy.data.curves.get('Temp').splines.clear()
        # self.GP_merge_as_layer('Temp_GP')

        # Styles are applied per Edge which we are looping over.
        # self.apply_styles(GP_materials, Edge_dom)  # FIXME RE-ADD

        # Hide invisible layers
        # if DOMLayer.getAttribute('visible') == 'false':
        #     bpy.data.grease_pencils[-1].layers[-1].hide = True
        # Assign layer their proper name
        # bpy.data.grease_pencils[-1].layers[-1].info = DOMLayer.getAttribute('name')  # FIXME RE-ADD
        # self.GP_merge_as_layer(GP_data)

    def apply_styles(self, GP_materials, Edge_dom):
        """
        Styles (color of the fills & strokes) are applied per <Edge/>.

        :param GP_materials: Grease pencil materials
        :param Edge_dom: The "<Edge/>" to apply the style from.
        :return: None
        """

        # TODO Assign materials/Styles
        fill0 = Edge_dom.getAttribute('fillStyle0')
        fill1 = Edge_dom.getAttribute('fillStyle1')
        stroke = Edge_dom.getAttribute('strokeStyle')

        if fill1:
            bpy.context.active_object.data.materials.append(GP_materials[fill1])
        if fill0:
            bpy.context.active_object.data.materials.append(GP_materials[fill0])
        if stroke:
            bpy.context.active_object.data.materials.append(GP_materials[stroke])
        bpy.ops.gpencil.stroke_change_color()

    def __init__(self, xml_file):
        """
        :param xml_file: The XML symbol file to be parsed & created.
        """
        XML = parse(xml_file)

        self.name = XML.getElementsByTagName('DOMSymbolItem')[0].getAttribute('name').replace('_', ' ')
        self.itemID = XML.getElementsByTagName('DOMSymbolItem')[0].getAttribute('itemID')
        self.symbolType = XML.getElementsByTagName('DOMSymbolItem')[0].getAttribute('symbolType')
        self.collection = bpy.data.collections.new(self.name + ' Symbols')
        bpy.context.scene.collection.children.link(self.collection)

        GP_data_block = bpy.data.grease_pencils.new(self.name)
        GP_Object = bpy.data.objects.new(self.name, GP_data_block)
        self.collection.objects.link(GP_Object)

        print(F"\n----INFO----\n"
              F"Symbol name: {self.name}\n"

              F"ID: {self.itemID}\n"
              F"Symbol Type: {self.symbolType}\n")
        # curve = bpy.data.curves.new(name='Temp', type='CURVE')
        # ob = bpy.data.objects.new(name='Temp', object_data=curve)
        # bpy.context.collection.objects.link(ob)

        print('------- New Grease pencil/Symbol/DOMShape -------')


        for timeline in XML.getElementsByTagName('timeline'):
            for DOMLayer in timeline.getElementsByTagName('DOMLayer'):
                layer_name = DOMLayer.getAttribute('name')
                print('|-Layer:', layer_name)
                GP_data_block.layers.new(layer_name)
                for DOMFrame in DOMLayer.getElementsByTagName('DOMFrame'):
                    print('||-Frame:', DOMFrame.getAttribute('index'))
                    for DOMShape in DOMFrame.getElementsByTagName('DOMShape'):
                        self.strokes(DOMShape, DOMLayer, self.name)
                    # break
                # break
            # break

        bpy.ops.object.mode_set()
        # bpy.data.objects.remove(bpy.data.objects['Temp'])
        # bpy.data.curves.remove(curve=bpy.data.curves.get('Temp'))

    def render_symbols(self):
        for symbol in self.symbols:
            symbol.add_svg_elements()
            # If symbol/data-block already exists then add a object with the data-block associated,
            # else create the data block and add


Lib = 'xml_symbols PATH'

# XML_file = Lib + 'CG_Hair_23.xml'
reset_blend()

import os

# Lib = '/home/user/Documents/Programming_projects/Python/Blend/xml_symbols'
# filenames = os.listdir(Lib)[1:5]

bpy.ops.object.add(type='CURVE', enter_editmode=True)
bpy.context.active_object.name = 'Temp'
bpy.context.active_object.data.name = 'Temp'

for filename in os.listdir(Lib)[:1]:
    # filename = Lib + 'Same_colour_fills_seperate_With_curves.xml'
    # filename = os.listdir(Lib)
    if filename.endswith(".xml"):
        print("New Symbol: ", filename)
        XFL_Symbol(xml_file=os.path.join(Lib, filename))

bpy.data.curves.remove(bpy.data.curves[-1])
# Started 11th of july, worked more or less every day I thought this was going to take a week.
#
#  A Poem by Anon.
#
#  From one anon to anon(ther).
#
#  Past selves spite me with discontent.
#  Anon., living the nightmare, dreaming of ponies. Dammed and forgotten by himself.
#  Go the right direction and then the distance.
#  Seek answers, indefinitely, for your happiness lies beyond the absolute impossible.
#  I dare. Do (you)?
#
#  ~ Anon.

"""
For strokes:
Each opcode is a stroke. ( Possible to optimize )

For fills:
Loop over every fill type
    Loop over fillstyle1 and fillstyle0 
        Find loops within edges 


Blender bpy comamnds:

bpy.ops.object.convert(target='GPENCIL', keep_original=True)
bpy.ops.gpencil.layer_duplicate_object(object="GPencil")
bpy.ops.object.delete(use_global=False)
bpy.ops.gpencil.stroke_join(type='JOIN')
bpy.context.object.data.active_index = 0
bpy.ops.gpencil.layer_merge()


"""
